extends Camera2D

export var edge_sensitivity = 64
export var pan_speed = 32

var tilemap
var world
var panning_time
var pan_target
var pan_start

func _ready():
	tilemap = get_parent()
	world = tilemap.get_parent()

func _process(delta):
	if pan_target:
		pan_to_target(delta)
		
	if world.ui.is_prompting() || world.duel_stage.visible:
		return
		
	pan_to_mouse(delta)
	
	#TODO limit to keep current player on screen?
		
	var map_rect = tilemap.get_used_rect()
	limit_y(map_rect)
	limit_x(map_rect)
	
func pan_to_mouse(delta):
	var mouse_vp = get_viewport().get_mouse_position()
	var pan_vel = Vector2(0,0)
	
	if mouse_vp.x <= edge_sensitivity:
		pan_vel.x -= 1
		pan_target = null
	elif mouse_vp.x >= (get_viewport().size.x - edge_sensitivity):
		pan_vel.x += 1
		pan_target = null
	
	if mouse_vp.y <= edge_sensitivity:
		pan_vel.y -= 1
		pan_target = null
	elif mouse_vp.y >= (get_viewport().size.y - edge_sensitivity):
		pan_vel.y += 1
		pan_target = null
		
	position += pan_vel.normalized() * pan_speed * delta

func limit_y(map_rect):
	var y_max = map_rect.size.y * tilemap.cell_size.y
	y_max -= get_viewport().size.y * zoom.y
	
	var buffer = get_viewport().size.y * zoom.y * 0.5
	
	if position.y < -buffer:
		position.y = -buffer
	elif position.y > y_max + buffer:
		position.y = y_max + buffer
	
func limit_x(map_rect):
	var x_max = map_rect.size.x * tilemap.cell_size.x
	x_max -= get_viewport().size.x * zoom.x
	
	var buffer = get_viewport().size.x * zoom.x * 0.5
	
	if position.x < -buffer:
		position.x = -buffer
	elif position.x > x_max + buffer:
		position.x = x_max + buffer
		
func is_panning():
	return pan_target != null
	
func pan_to_position(pos):
	pos.x -= get_viewport().size.x * 0.5 * zoom.x
	pos.y -= get_viewport().size.y * 0.5 * zoom.y
	position = pos
	pan_target = null
		
func pan_to_target(delta):
	var pos = pan_target.position
	pos.x -= get_viewport().size.x * 0.5 * zoom.x
	pos.y -= get_viewport().size.y * 0.5 * zoom.y
	position = pos

func pan_to_player(player):
	panning_time = 0
	pan_target = player
	pan_start = position
	