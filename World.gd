extends Node2D

export (PackedScene)var rock
export (PackedScene)var skull
export (PackedScene)var cactus

export var debris_per_player = 4
export var gold_4_per_player = 2
export var gold_3_per_player = 3
export var gold_2_per_player = 4
export var gold_1_per_player = 5

var selected_tile

var next_player_id = 0
var current_player_id
var in_play = {}
var turn_order
var current_turn
var camera
var tilemap
var ui
var primary_player

var duel_stage
var duel_challenger
var duel_location
var duel_target
var duel_winner
var duel_loser
var forfeits

var scores

var initialized = false

func register_player_id(player):
	var id = next_player_id
	next_player_id += 1
	in_play[id] = player
	return id
	
func set_player(player):
	primary_player = player
	if ui:
		ui.set_player_name(primary_player.name)
	
func set_score(score):
	scores[primary_player.id] = score
	ui.set_score(score)
	
func prompt_to_start_duel(challenger, target, loc):
	duel_challenger = challenger
	duel_target = target
	ui.prompt_to_start_duel()
	camera.pan_to_position(loc)
	duel_location = tilemap.world_to_map(loc)
	
func send_duel_challenge():
	if duel_target.accept_duel(duel_challenger, duel_location):
		ui.start_challenge_prompt.hide()
		start_duel()
	else:
		ui.start_challenge_prompt.hide()
		ui.show_rejected(duel_target.name)
		
func handle_rejection():
	duel_challenger.handle_duel_won()
	duel_challenger = null
	duel_target = null
		
func start_duel():
	duel_stage.prepare(duel_challenger, duel_target)
	
func win_duel(id):
	duel_winner = id
		
	if duel_challenger.id == id:
		duel_loser = duel_target.id
			
		ui.show_duel_results(duel_challenger, duel_target)
		duel_challenger.handle_duel_won()
		duel_target.queue_free()
	else:
		duel_loser = duel_challenger.id
		
		ui.show_duel_results(duel_target, duel_challenger)
		duel_challenger.queue_free()
	
	if duel_loser == primary_player.id:
		primary_player = null
	
func complete_duel():
	if primary_player == null:
		ui.show_duel_loss()
	elif duel_winner == duel_challenger.id:
		duel_challenger.handle_duel_won()
	else:
		duel_target.handle_duel_won()
		
	duel_stage.clear()
	duel_stage.hide()
	tilemap.clear_ownership(duel_loser)
	in_play[duel_loser] = null

func forfeit_duel(id):
	if forfeits[id]:
		duel_stage.hide()
		ui.show_forfeit_loss()
	else:
		duel_stage.clear()
		ui.show_forfeit_warning()
		forfeits[id] = true

func reset_duel():
	duel_stage.show()
	duel_stage.prepare(duel_challenger, duel_target)
	
func _process(delta):
	if !initialized:
		tilemap = get_node("TileMap")
		camera = get_node("TileMap/Camera2D")
		ui = get_node("TileMap/Camera2D/CanvasLayer/UI")
		duel_stage = get_node("TileMap/Camera2D/DuelingStage")
		duel_stage.hide()
		
		if primary_player:
			ui.set_player_name(primary_player.name)
			
		forfeits = []
		scores = []
		for i in range(in_play.keys().size()):
			forfeits.append(false)
			scores.append(0)
			
		#place gold randomly
		randomize()
		var player_count = in_play.keys().size()
		
		for i in range(player_count * gold_4_per_player):
			var spot = pick_gold_spot()
			tilemap.place_gold(spot, 4)
			
		for i in range(player_count * gold_3_per_player):
			var spot = pick_gold_spot()
			tilemap.place_gold(spot, 3)
			
		for i in range(player_count * gold_2_per_player):
			var spot = pick_gold_spot()
			tilemap.place_gold(spot, 2)
			
		for i in range(player_count * gold_1_per_player):
			var spot = pick_gold_spot()
			tilemap.place_gold(spot, 1)
		
		#spawn debris
		var total_debris = debris_per_player * player_count
		for n in range(total_debris):
			var spot = pick_debris_spot()
			if !tilemap.has_debris(spot):
				var type = randi() % 3
				var thing
				match type:
					0:
						thing = rock.instance()
					1:
						thing = skull.instance()
					2:
						thing = cactus.instance()
						
				tilemap.add_debris(spot, thing)
		
		#Set up player's homes
		#TODO spawn players at a selection of predetermined spawn points
		for id in in_play.keys():
			var player = in_play[id]
			player.make_yourself_at_home()
			
		turn_order = []
		var turns = in_play.keys()
		for i in range(turns.size()):
			var turn_index = randi()%turns.size()
			turn_order.append(turns[turn_index])
			turns.remove(turn_index)
			
		current_turn = -1
		start_next_turn()
		initialized = true
		
func start_next_turn():
	if tilemap.all_owned_by(primary_player.id, true):
		var score = scores[primary_player.id]
		ui.set_winner(primary_player.name, score)
	elif tilemap.count_unowned_space() == 0:
		var report = tilemap.gold_report(in_play.keys())
		var winning_id = -1
		var high_score = 0
		for id in report.keys():
			var score = report[id]
			if score > high_score:
				winning_id = id
				high_score = score
		ui.set_winner(in_play[winning_id].name, high_score)
	else:
		current_turn += 1
		if current_turn >= turn_order.size():
			current_turn = 0
			
		var zero_once = current_turn == 0
		current_player_id = turn_order[current_turn]
		while in_play[current_player_id] == null:
			current_turn += 1
			if current_turn >= turn_order.size():
				current_turn = 0
				if zero_once:
					break
				else:
					zero_once = true
			current_player_id = turn_order[current_turn]
			
		ui.start_turn(in_play[current_player_id].name)
		camera.pan_to_player(in_play[current_player_id])
	
func is_it_my_turn(id):
	return current_player_id == id && !ui.is_prompting() && !duel_stage.visible
	
func end_turn():
	start_next_turn()
	
func pick_gold_spot():
	#TODO find a new spot when it has gold
	var spot = tilemap.random_spot()
	return spot
	
func pick_debris_spot():
	#TODO find a new spot when it has debris
	var spot = tilemap.random_spot()
	return spot