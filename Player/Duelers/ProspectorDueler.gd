extends Node2D

var stage
var id

func _ready():
	stage = get_parent()
	
func _process(delta):
	if should_fire():
		bang()
		
func should_fire():
	return Input.is_action_just_pressed("pick_tile")

func bang():
	if !stage.over:
		scale.x = -scale.x
		stage.bang(id)
	
func ready():
	pass