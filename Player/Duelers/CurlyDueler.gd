extends "res://Player/Duelers/ProspectorDueler.gd"

export var delay_base = 0.5

export var delay_variance = 0.25

var delay

var started = false
var elapsed = 0

func _ready():
	delay = delay_base + rand_range(-delay_variance, delay_variance)
	
func _process(delta):
	if started:
		elapsed += delta

func should_fire():
	return elapsed >= delay

func ready():
	started = true