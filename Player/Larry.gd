extends "res://Player/CPUPlayer.gd"

export var challenge_limit = 3

var challenges = 0

func hate(challenger):
	challenges += 1
	return challenges > challenge_limit

func greedy(challenger):
	return tilemap.get_territory_count(id) <= 4

func determine_destination(pos):
	#BFS for onowned tile
	var to_check = [pos]
	var checked = []
	var gold_values = {0 : [], 1 : [], 2 : [], 3 : [], 4 : []}
	while to_check.size() > 0:
		var checking = to_check.pop_front()
		if !checked.has(checking):
			checked.append(checking)
			if tilemap.get_used_rect().has_point(checking):
				var tile_owner = tilemap.get_owner(checking)
				if tile_owner == tilemap.no_owner:
					var value = tilemap.gold_value(checking)
					gold_values[value].append(checking)
				elif tile_owner == id:
					var north = checking + Vector2(0, -1)
					var south = checking + Vector2(0, 1)
					var west = checking + Vector2(-1, 0)
					var east = checking + Vector2(1, 0)
					if !checked.has(north):
						to_check.append(north)
					if !checked.has(south):
						to_check.append(south)
					if !checked.has(east):
						to_check.append(east)
					if !checked.has(west):
						to_check.append(west)
	#prioritize gold
	if gold_values[4].size() > 0:
		return gold_values[4][0]
	elif gold_values[3].size() > 0:
		return gold_values[3][0]
	elif gold_values[2].size() > 0:
		return gold_values[2][0]
	elif gold_values[1].size() > 0:
		return gold_values[1][0]
	elif gold_values[0].size() > 0:
		return gold_values[0][0]
		
	return null