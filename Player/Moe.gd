extends "res://Player/CPUPlayer.gd"

var gold_values = null

var path_to_gold = []

var challenges = []

func hate(challenger):
	if !challenges.has(challenger.id):
		challenges.append(challenger.id)
		return false
	else:
		return true
	
func greedy(challenger):
	var my_score = tilemap.get_territory_count(id)
	var their_score = tilemap.get_territory_count(challenger.id)
	
	return my_score < their_score


func determine_destination(pos):
	if gold_values == null:
		locate_gold(pos)
		
	if path_to_gold.size() > 0 && check_current_path():
		var step = path_to_gold.pop_front()
		
		#take the next valid step
		while tilemap.get_owner(step) == id && path_to_gold.size() > 0:
			step = path_to_gold.pop_front()
				
		if step != null && tilemap.get_owner(step) == -1:
			return step
			
	var to_claim = determine_goal(pos)
	while to_claim != null && tilemap.get_owner(to_claim) == id && path_to_gold.size() > 0:
		to_claim = path_to_gold.pop_front()
		
	return to_claim

func check_goal(pos, goal):
	if goal != null:
		var path_to_check = tilemap.path_through_territory(id, pos, goal, true)
		if path_to_check.size() > 0:
			path_to_gold = path_to_check
			return true
	return false
		
func check_current_path():
	for i in range(path_to_gold.size()):
		var owner = tilemap.get_owner(path_to_gold[i])
		if owner != -1 && owner != id:
			path_to_gold = []
			return false
	return true

func determine_goal(pos):
	var to_claim = null
	
	for i in range(gold_values[4].size()):
		var loc = gold_values[4][i]
		if !tilemap.is_owned(loc):
			if check_goal(pos, loc):
				to_claim = loc
				break
	
	if to_claim == null:
		for i in range(gold_values[3].size()):
			var loc = gold_values[3][i]
			if !tilemap.is_owned(loc):
				if check_goal(pos, loc):
					to_claim = loc
					break
	
	if to_claim == null:
		for i in range(gold_values[2].size()):
			var loc = gold_values[2][i]
			if !tilemap.is_owned(loc):
				if check_goal(pos, loc):
					to_claim = loc
					break
	
	if to_claim == null:
		for i in range(gold_values[1].size()):
			var loc = gold_values[1][i]
			if !tilemap.is_owned(loc):
				if check_goal(pos, loc):
					to_claim = loc
					break
	
	if to_claim == null:
		for i in range(gold_values[0].size()):
			var loc = gold_values[0][i]
			if !tilemap.is_owned(loc):
				if check_goal(pos, loc):
					to_claim = loc
					break
	
	return to_claim

func locate_gold(pos):
	gold_values = [[],[],[],[],[]]
	#BFS for onowned tile
	var to_check = [pos]
	var checked = []
	while to_check.size() > 0:
		var checking = to_check.pop_front()
		if !checked.has(checking):
			checked.append(checking)
			if tilemap.get_used_rect().has_point(checking):
				var tile_owner = tilemap.get_owner(checking)
				if tile_owner == tilemap.no_owner:
					var value = tilemap.gold_value(checking)
					gold_values[value].append(checking)
					
				var north = checking + Vector2(0, -1)
				var south = checking + Vector2(0, 1)
				var west = checking + Vector2(-1, 0)
				var east = checking + Vector2(1, 0)
				if !checked.has(north):
					to_check.append(north)
				if !checked.has(south):
					to_check.append(south)
				if !checked.has(east):
					to_check.append(east)
				if !checked.has(west):
					to_check.append(west)
	#now gold_values is the whole map