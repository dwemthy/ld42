extends "res://Player/CPUPlayer.gd"

var challenges = []

func hate(challenger):
	if !challenges.has(challenger.id):
		challenges.append(challenger.id)
		return false
	else:
		return true