extends Node2D

export(PackedScene) var claim
export(PackedScene) var home
export(PackedScene) var dueler
export var speed = 2048

var tilemap
var world
var id

var path = null
var destination = null
var turns_taken
var done
var waiting
var end_of_turn_wait = 0.5

func _enter_tree():
	tilemap = get_parent()
	world = tilemap.get_parent()
	
func _ready():
	waiting = 0
	register()

func register():
	var global = get_node("/root/Global")
	name = global.player_name
	id = world.register_player_id(self)
	world.set_player(self)

func _process(delta):
	if world.is_it_my_turn(id):
		if done:
			waiting += delta
			if waiting >= end_of_turn_wait:
				world.start_next_turn()
		elif destination:
			move_towards_destination(delta)
		else:
			find_destination(delta)
	else:
		done = false
		waiting = 0
			
func move_towards_destination(delta):
	if position.x > destination.x:
		position.x -= speed * delta
		if position.x < destination.x:
			position.x = destination.x 
	elif position.x < destination.x:
		position.x += speed * delta
		if position.x > destination.x:
			position.x = destination.x
	elif position.y < destination.y:
		position.y += speed * delta
		if position.y > destination.y:
			position.y = destination.y
	elif position.y > destination.y:
		position.y -= speed * delta
		if position.y < destination.y:
			position.y = destination.y
	else:
		step_along_path()
				
func find_destination(delta):
	if Input.is_action_just_pressed("pick_tile"):
		attempt_set_destination(world.selected_tile)
	elif Input.is_action_just_pressed("pass"):
		world.start_next_turn()

func attempt_set_destination(tile_location):
	if tilemap.can_claim(tile_location, id):
		path = tilemap.path_through_territory(id, tilemap.world_to_map(position), tile_location)
		step_along_path()
	elif tilemap.can_duel(tile_location, id):
		var target_id = tilemap.get_owner(tile_location)
		var target = world.in_play[target_id]
		if target != null:
			duel_location = tile_location
			var duel_pos = tilemap.map_to_world(duel_location)
			duel_pos.x += tilemap.cell_size.x * 0.5
			duel_pos.y += tilemap.cell_size.y * 0.5
			world.prompt_to_start_duel(self, target, duel_pos)
		
var duel_location
func handle_duel_won():
	path = tilemap.path_through_territory(id, tilemap.world_to_map(position), duel_location)
	step_along_path()
	
func step_along_path():
	if path.size() > 0:
		var next_step = path.pop_front()
		if tilemap.has_debris(next_step) && !tilemap.get_owner(next_step) == id:
			tilemap.clear_debris(next_step)
			path.clear()
			destination = null
			if tilemap.get_owner(next_step) != -1:
				tilemap.set_owner(next_step, -1, null)
			done = true
		else:
			destination = tilemap.map_to_world(next_step)
			destination.x += tilemap.cell_size.x * 0.5
			destination.y += tilemap.cell_size.y * 0.5
	elif destination != null:
		var tile_dest = tilemap.world_to_map(destination)
		if tilemap.has_debris(tile_dest) && !tilemap.get_owner(tile_dest) == id:
			tilemap.clear_debris(tile_dest)
			path.clear()
			destination = null
			if tilemap.get_owner(tile_dest) != -1:
				tilemap.set_owner(tile_dest, -1, null)
			done = true
		else:
			var tile_pos = tilemap.world_to_map(destination)
			claim(destination)
			path.clear()
			destination = null
			done = true
	
func make_yourself_at_home():
	var house = home.instance()
	var map_pos = tilemap.world_to_map(position)
	var house_pos = tilemap.map_to_world(map_pos)
	house_pos.x += tilemap.cell_size.x * 0.5
	house_pos.y += tilemap.cell_size.y * 0.5
	house.position = house_pos
	position = house.position#center self on tile too
	
	if tilemap.has_debris(map_pos):
		tilemap.clear_debris(map_pos)
		
	tilemap.add_debris(map_pos, house)
	claim(position)
	
func claim(world_pos):
	var tile_loc = tilemap.world_to_map(world_pos)
	var indicator = claim.instance()
	tilemap.set_owner(tile_loc, id, indicator)
	celebrate()
	
func celebrate():
	var score = tilemap.sum_gold(id)
	world.set_score(score)
	
func accept_duel(challenger, location):
	return true