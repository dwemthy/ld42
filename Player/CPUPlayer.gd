extends "res://Player/Prospector.gd"

var start_delay = 0
var delayed = 0

func register():
	id = world.register_player_id(self)

func celebrate():
	pass

func find_destination(delta):
	delayed += delta
	if delayed < start_delay:
		return
		
	var my_pos = tilemap.world_to_map(position)
	var target = determine_destination(my_pos)
	if target != null:
		delayed = 0
		attempt_set_destination(target)
	else:
		world.start_next_turn()
	
func determine_destination(pos):
	#BFS for onowned tile
	var to_check = [pos]
	var checked = []
	while to_check.size() > 0:
		var checking = to_check.pop_front()
		if !checked.has(checking):
			checked.append(checking)
			if tilemap.get_used_rect().has_point(checking):
				var tile_owner = tilemap.get_owner(checking)
				if tile_owner == tilemap.no_owner:
					return checking
				elif tile_owner == id:
					var north = checking + Vector2(0, -1)
					var south = checking + Vector2(0, 1)
					var west = checking + Vector2(-1, 0)
					var east = checking + Vector2(1, 0)
					if !checked.has(north):
						to_check.append(north)
					if !checked.has(south):
						to_check.append(south)
					if !checked.has(east):
						to_check.append(east)
					if !checked.has(west):
						to_check.append(west)
	return null
	

func accept_duel(challenger, location):
	if last_space() ||  home_space(location) || personal_space(location):
		return true
		
	if pity(challenger):
		return false
		
	return hate(challenger) || greedy(challenger)

func pity(challenger):
	return false

func hate(challenger):
	return true
	
func greedy(challenger):
	return false
	
func last_space():
	return tilemap.get_territory_count(id) == 1
	
func home_space(location):
	return tilemap.has_debris(location)
	
func personal_space(location):
	return location == tilemap.world_to_map(position)
	