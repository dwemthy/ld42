extends AnimatedSprite

var tilemap
var world

func _ready():
	tilemap = get_node("/root/World/TileMap")
	world = get_node("/root/World")
	
func _process(delta):
	var mouse_pos = get_global_mouse_position()
	var tile_location = tilemap.world_to_map(mouse_pos)
	var rect = tilemap.get_used_rect()
	if world.primary_player == null || world.primary_player.id != world.current_player_id:
		hide()
		return
	else:
		show()
		
	if rect.has_point(tile_location) && !world.ui.is_prompting():
		show()
			
		world.selected_tile = tile_location
		position = tilemap.map_to_world(tile_location)
		if tilemap.can_claim(world.selected_tile, world.current_player_id):
			if tilemap.has_debris(world.selected_tile):
				frame = 3
			else:
				frame = 0
		elif tilemap.can_duel(world.selected_tile, world.current_player_id):
			frame = 2
		else:
			frame = 1
	elif world.ui.start_challenge_prompt.visible:
		hide()