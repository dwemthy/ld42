extends TextureButton

var world

func _ready():
	world = get_node("/root/World")
	connect("button_down", self, "pass_pressed")
	
func pass_pressed():
	if world.current_player_id == world.primary_player.id:
		get_node("/root/World").start_next_turn()