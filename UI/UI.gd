extends ViewportContainer

var pass_button
var current_turn
var winner_name
var winner_amount
var end_panel
var name_label
var gold_count
var start_challenge_prompt
var initiate_duel_button
var nevermind_duel_button
var duel_counter
var duel_instructions
var forfeit_warning
var duel_loss
var restart_button
var warning_ok
var duel_result
var duel_winner
var duel_loser
var result_ok
var duel_rejected
var rejected_target
var rejected_ok

var world

func _ready():
	world = get_node("/root/World")
	pass_button = get_node("PassButton")
	current_turn = get_node("TurnSign/CurrentTurn")
	end_panel = get_node("WinPanel")
	winner_name = get_node("WinPanel/WinnerName")
	winner_amount = get_node("WinPanel/ScoreLabel")
	name_label = get_node("NamePlate/NameLabel")
	gold_count = get_node("CartPlate/GoldCount")
	start_challenge_prompt = get_node("DuelChallengeStart")
	initiate_duel_button = get_node("DuelChallengeStart/Challenge")
	initiate_duel_button.connect("button_up", self, "initiate_duel")
	nevermind_duel_button = get_node("DuelChallengeStart/NeverMind")
	nevermind_duel_button.connect("button_up", self, "nevermind_duel")
	duel_instructions = get_node("DuelInstructions")
	duel_counter = get_node("DuelCounter")
	forfeit_warning = get_node("ForfeitWarning")
	duel_loss = get_node("DuelLoss")
	warning_ok = get_node("ForfeitWarning/OkButton")
	warning_ok.connect("button_up", self, "resume_duel")
	restart_button = get_node("RestartButton")
	restart_button.connect("button_up", self, "restart")
	duel_result = get_node("DuelResult")
	duel_winner = get_node("DuelResult/WinnerLabel")
	duel_loser = get_node("DuelResult/LoserLabel")
	result_ok = get_node("DuelResult/OkButton")
	result_ok.connect("button_up", self, "complete_duel")
	duel_rejected = get_node("DuelRejected")
	rejected_target = get_node("DuelRejected/TargetLabel")
	rejected_ok = get_node("DuelRejected/OkButton")
	rejected_ok.connect("button_down", self, "rejected")
	
	end_panel.hide()
	
func show_rejected(target_name):
	rejected_target.text = target_name
	duel_rejected.show()
	
func rejected():
	duel_rejected.hide()
	world.handle_rejection()
	
func resume_duel():
	forfeit_warning.hide()
	world.reset_duel()
	
func complete_duel():
	duel_result.hide()
	world.complete_duel()
	
func restart():
	get_tree().change_scene("res://Start.tscn")
	
func initiate_duel():
	world.send_duel_challenge()
	
func nevermind_duel():
	start_challenge_prompt.hide()
	
func show_duel_results(winner, loser):
	duel_result.show()
	duel_winner.text = winner.name
	duel_loser.text = loser.name
	
func is_prompting():
	return start_challenge_prompt.visible || end_panel.visible || duel_result.visible || duel_loss.visible || forfeit_warning.visible
	
func prompt_to_start_duel():
	start_challenge_prompt.show()
	
func set_player_name(player_name):
	name_label.text = player_name

func show_duel_count(count):
	duel_counter.show()
	duel_counter.text = String(count)
	duel_instructions.show()

func hide_duel_count():
	duel_counter.hide()
	duel_instructions.hide()
	
func show_forfeit_loss():
	duel_result.hide()
	duel_loss.show()
	restart_button.show()

func show_duel_loss():
	duel_result.hide()
	duel_loss.show()
	restart_button.show()

func show_forfeit_warning():
	forfeit_warning.show()
	
func set_score(score):
	gold_count.text = String(score)
	
func start_turn(turn_name):
	current_turn.text = turn_name + "'s Turn"
	
func set_winner(player, score):
	winner_name.text = player
	winner_amount.text = "Won with " + String(score) + " gold"
	end_panel.show()
	restart_button.show()