extends Node2D

export var time_per_count = 1
export var min_time_til_draw = 0.5
export var max_time_til_draw = 5.0

var world

var aggressor_start
var aggressor_1
var aggressor_2
var aggressor_3
var defender_start
var defender_1
var defender_2
var defender_3

var aggressor
var defender

var elapsed = 0
var on_count = 0
var time_til_draw

var over

func _ready():
	world = get_node("/root/World")
	
	aggressor_start = get_node("Aggressor")
	aggressor_1 = get_node("Aggressor1")
	aggressor_2 = get_node("Aggressor2")
	aggressor_3 = get_node("Aggressor3")
	
	defender_start = get_node("Defender")
	defender_1 = get_node("Defender1")
	defender_2 = get_node("Defender2")
	defender_3 = get_node("Defender3")
	hide()
	
func _process(delta):
	if !visible || aggressor == null || defender == null || over:
		return
		
	elapsed += delta
	
	if on_count < 3:
		if elapsed > time_per_count:
			elapsed = 0
			on_count += 1
			show_count(on_count)
		else:
			var percent = elapsed * 1.5
			if percent > 1:
				percent = 1
				
			var agg_pos
			var def_pos
				
			match on_count:
				0:
					agg_pos = aggressor_start.position + ((aggressor_1.position - aggressor_start.position) * percent)
					def_pos = defender_start.position + ((defender_1.position - defender_start.position) * percent)
				1:
					agg_pos = aggressor_1.position + ((aggressor_2.position - aggressor_1.position) * percent)
					def_pos = defender_1.position + ((defender_2.position - defender_1.position) * percent)
				2:
					agg_pos = aggressor_2.position + ((aggressor_3.position - aggressor_2.position) * percent)
					def_pos = defender_2.position + ((defender_3.position - defender_2.position) * percent)
					
			aggressor.position = agg_pos
			defender.position = def_pos
				
	elif on_count == 3:
		if elapsed >= time_til_draw:
			on_count += 1
			show_draw()

func show_count(i):
	world.ui.show_duel_count(i)

func show_draw():
	world.ui.show_duel_count("DRAW")
	aggressor.ready()
	defender.ready()

func bang(id):
	if !over:
		over = true
		if elapsed >= time_til_draw:
			world.win_duel(id)
		else:
			world.forfeit_duel(id)
	
func prepare(challenger, target):
	aggressor = challenger.dueler.instance()
	aggressor.position = aggressor_start.position
	aggressor.scale.x = -aggressor.scale.x
	aggressor.id = challenger.id
	add_child(aggressor)
	
	defender = target.dueler.instance()
	defender.position = defender_start.position
	defender.id = target.id
	add_child(defender)
	
	over = false
	on_count = 0
	time_til_draw = rand_range(min_time_til_draw, max_time_til_draw)
	world.ui.show_duel_count("Ready...")
	show()
	
func clear():
	if aggressor != null:
		aggressor.queue_free()
		aggressor = null
	
	if defender != null:
		defender.queue_free()
		defender = null
		
	world.ui.hide_duel_count()