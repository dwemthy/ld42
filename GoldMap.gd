extends TileMap

var tile_owners
var owner_markers
var debris
var gold_cache = {}

const no_owner = -1

func _enter_tree():
	tile_owners = []
	debris = []
	owner_markers = []
	for x in range(get_used_rect().size.x):
		tile_owners.append([])
		debris.append([])
		owner_markers.append([])
		for y in range(get_used_rect().size.y):
			tile_owners[x].append(no_owner)
			debris[x].append(null)
			owner_markers[x].append(null)

func is_owned(pos):
	return tile_owners[pos.x][pos.y] != no_owner
	
func get_owner(pos):
	if !get_used_rect().has_point(pos):
		return -2
	return tile_owners[pos.x][pos.y]

func has_debris(pos):
	return debris[pos.x][pos.y] != null
	
func clear_debris(pos):
	debris[pos.x][pos.y].queue_free()
	debris[pos.x][pos.y] = null
	
func random_y():
	return rand_range(get_used_rect().position.y, get_used_rect().position.y + get_used_rect().size.y)
	
func random_x():
	return rand_range(get_used_rect().position.x, get_used_rect().position.x + get_used_rect().size.x)
	
func random_spot():
	var x = random_x()
	var y = random_y()
	return Vector2(x,y)
	
func add_debris(pos, item):
	debris[pos.x][pos.y] = item
	add_child(item)
	var world_pos = map_to_world(pos)
	world_pos.x += cell_size.x * 0.5
	world_pos.y += cell_size.y * 0.5
	item.position = world_pos

func gold_value(pos):
	var gold = 0
	if gold_cache.has(pos):
		gold = gold_cache[pos]
	else:
		var cell = get_cellv(pos)
		var tile_name = tile_set.tile_get_name(cell)
		match (tile_name):
			"0Gold":
				gold = 0
			"1Gold":
				gold = 1
			"2Gold":
				gold = 2
			"3Gold":
				gold = 3
			"4Gold":
				gold = 4
		gold_cache[pos] = gold
	return gold
	
func place_gold(pos, amount):
	gold_cache[pos] = amount
	match(amount):
		0:
			set_cellv(pos, tile_set.find_tile_by_name("0Gold"))
		1:
			set_cellv(pos, tile_set.find_tile_by_name("1Gold"))
		2:
			set_cellv(pos, tile_set.find_tile_by_name("2Gold"))
		3:
			set_cellv(pos, tile_set.find_tile_by_name("3Gold"))
		4: 
			set_cellv(pos, tile_set.find_tile_by_name("4Gold"))
	
func set_owner(pos, owner_id, indicator):
	tile_owners[pos.x][pos.y] = owner_id
	
	if indicator != null:
		add_child(indicator)
		indicator.position = map_to_world(pos)
		
	if owner_markers[pos.x][pos.y] != null:
		owner_markers[pos.x][pos.y].queue_free()
		
	owner_markers[pos.x][pos.y] = indicator
	
func all_owned_by(id, or_unclaimed = false):
	for x in range(tile_owners.size()):
		for y in range(tile_owners[x].size()):
			#when unclaimed is true this will not return true for no_owner tiles
			if tile_owners[x][y] != id && (!or_unclaimed || tile_owners[x][y] != no_owner):
				return false
	return true
	
func clear_ownership(id):
	for x in range(tile_owners.size()):
		for y in range(tile_owners[x].size()):
			if tile_owners[x][y] == id:
				tile_owners[x][y] = no_owner
				owner_markers[x][y].queue_free()
				owner_markers[x][y] = null
	
func can_claim(loc, owner):
	if loc.x >= tile_owners.size() || loc.y >= tile_owners[loc.x].size():
		return false
	elif tile_owners[loc.x][loc.y] != no_owner:
		return false
	elif loc.x > 0 && tile_owners[loc.x - 1][loc.y] == owner:
		return true
	elif loc.y > 0 && tile_owners[loc.x][loc.y - 1] == owner:
		return true
	elif loc.x < tile_owners.size() - 1 && tile_owners[loc.x + 1][loc.y] == owner:
		return true
	elif loc.y < tile_owners[0].size() - 1 && tile_owners[loc.x][loc.y + 1] == owner:
		return true
	else:
		return false
		
func can_duel(loc, owner):
	if loc.x >= tile_owners.size() || loc.y >= tile_owners[loc.x].size():
		return false
	elif tile_owners[loc.x][loc.y] == no_owner || tile_owners[loc.x][loc.y] == owner:
		return false
	elif loc.x > 0 && tile_owners[loc.x - 1][loc.y] == owner:
		return true
	elif loc.y > 0 && tile_owners[loc.x][loc.y - 1] == owner:
		return true
	elif loc.x < tile_owners.size() - 1 && tile_owners[loc.x + 1][loc.y] == owner:
		return true
	elif loc.y < tile_owners[0].size() - 1 && tile_owners[loc.x][loc.y + 1] == owner:
		return true
	else:
		return false
		
func get_territory_count(id):
	var count = 0
	for x in range(tile_owners.size()):
		for y in range(tile_owners[x].size()):
			if tile_owners[x][y] == id:
				count += 1
	return count
	
func count_unowned_space():
	return get_territory_count(no_owner)
	
func sum_gold(id):
	var count = 0
	for x in range(tile_owners.size()):
		for y in range(tile_owners[x].size()):
			if tile_owners[x][y] == id:
				count += gold_value(Vector2(x,y))
	return count
	
func gold_report(ids):
	var report = {}
	for id in ids:
		var count = sum_gold(id)
		report[id] = count
		
	return report

func ownership_report(ids):
	var report = {}
	for id in ids:
		var count = get_territory_count(id)
		report[id] = count
		
	return report
		
var visited
#Uses tile indices, not world pos
func path_through_territory(owner, start, end, or_unowned = false):
	visited = []
	for x in range(get_used_rect().size.x):
		visited.append([])
		for y in range(get_used_rect().size.y):
			visited[x].append(0)
			
	return _path_through_territory(owner, start, end, or_unowned)
	
func _path_through_territory(owner, start, end, or_unowned = false):
	#point is out of bounds or visited
	if !get_used_rect().has_point(start) || visited[start.x][start.y] > 0:
		return []
		
	visited[start.x][start.y] = 1
		
	var route = []
	if start.x == end.x && start.y == end.y:
		route = [end]
	elif get_owner(start) == owner || (get_owner(start) == no_owner && or_unowned):
		var max_x = get_used_rect().size.x - 1
		var max_y = get_used_rect().size.y - 1
		var diff = end - start
		
		### START long section of picking order of directions
		if abs(diff.x) > abs(diff.y):
			if diff.x > 0: 	#EAST FIRST
				var east_path = pathfind_east(owner, start, end, or_unowned)
				if east_path.size() > 0:
					route = east_path
				else:
					if diff.y > 0:
						var south_path = pathfind_south(owner, start, end, or_unowned)
						if south_path.size() > 0:
							route = south_path
						else :
							var north_path = pathfind_north(owner, start, end, or_unowned)
							if north_path.size() > 0:
								route = north_path
							else:
								var west_path = pathfind_west(owner, start, end, or_unowned)
								if west_path.size() > 0:
									route = west_path
								#else nothing, no more directions to check
					else:
						var north_path = pathfind_north(owner, start, end, or_unowned)
						if north_path.size() > 0:
							route = north_path
						else :
							var south_path = pathfind_south(owner, start, end, or_unowned)
							if south_path.size() > 0:
								route = south_path
							else:
								var west_path = pathfind_west(owner, start, end, or_unowned)
								if west_path.size() > 0:
									route = west_path
								#else nothing, no more directions to check
			else:		   	#WEST FIRST
				var west_path = pathfind_west(owner, start, end, or_unowned)
				if west_path.size() > 0:
					route = west_path
				else:
					if diff.y > 0:
						var south_path = pathfind_south(owner, start, end, or_unowned)
						if south_path.size() > 0:
							route = south_path
						else :
							var north_path = pathfind_north(owner, start, end, or_unowned)
							if north_path.size() > 0:
								route = north_path
							else:
								var east_path = pathfind_east(owner, start, end, or_unowned)
								if east_path.size() > 0:
									route = east_path
								#else nothing, no more directions to check
					else:
						var north_path = pathfind_north(owner, start, end, or_unowned)
						if north_path.size() > 0:
							route = north_path
						else :
							var south_path = pathfind_south(owner, start, end, or_unowned)
							if south_path.size() > 0:
								route = south_path
							else:
								var east_path = pathfind_east(owner, start, end, or_unowned)
								if east_path.size() > 0:
									route = east_path
								#else nothing, no more directions to check
		else:
			if diff.y > 0:	#SOUTH FIRST
				var south_path = pathfind_south(owner, start, end, or_unowned)
				if south_path.size() > 0:
					route = south_path
				else:
					if diff.x > 0: #EAST NEXT
						var east_path = pathfind_east(owner, start, end, or_unowned)
						if east_path.size() > 0:
							route = east_path
						else : #Then West
							var west_path = pathfind_west(owner, start, end, or_unowned)
							if west_path.size() > 0:
								route = west_path
							else: #Finally North
								var north_path = pathfind_north(owner, start, end, or_unowned)
								if north_path.size() > 0:
									route = north_path
								#else nothing, no more directions to check
					else: #WEST NEXT
						var west_path = pathfind_west(owner, start, end, or_unowned)
						if west_path.size() > 0:
							route = west_path
						else : #Then East
							var east_path = pathfind_east(owner, start, end, or_unowned)
							if east_path.size() > 0:
								route = east_path
							else: # Finally North
								var north_path = pathfind_north(owner, start, end, or_unowned)
								if north_path.size() > 0:
									route = north_path
								#else nothing, no more directions to check
			else: #NORTH FIRST
				var north_path = pathfind_north(owner, start, end, or_unowned)
				if north_path.size() > 0:
					route = north_path
				else: 
					if diff.x > 0: #EAST NEXT
						var east_path = pathfind_east(owner, start, end, or_unowned)
						if east_path.size() > 0:
							route = east_path
						else : #Then West
							var west_path = pathfind_west(owner, start, end, or_unowned)
							if west_path.size() > 0:
								route = west_path
							else: #Finally South
								var south_path = pathfind_south(owner, start, end, or_unowned)
								if south_path.size() > 0:
									route = south_path
								#else nothing, no more directions to check
					else: #WEST NEXT
						var west_path = pathfind_west(owner, start, end, or_unowned)
						if west_path.size() > 0:
							route = west_path
						else : # Then East
							var east_path = pathfind_east(owner, start, end, or_unowned)
							if east_path.size() > 0:
								route = east_path
							else: # Finally South
								var south_path = pathfind_south(owner, start, end, or_unowned)
								if south_path.size() > 0:
									route = south_path
								#else nothing, no more directions to check
		### END long section of picking order of directions
		
	visited[start.x][start.y] = 0
	return route
	
func pathfind_north(owner, start, end, or_unowned = false):
	var north = start
	north.y -= 1
	var path = _path_through_territory(owner, north, end, or_unowned)
	if path.size() > 0:
		path.push_front(start)
	return path

func pathfind_south(owner, start, end, or_unowned = false):
	var south = start
	south.y += 1
	var path = _path_through_territory(owner, south, end, or_unowned)
	if path.size() > 0:
		path.push_front(start)
	return path
	
func pathfind_east(owner, start, end, or_unowned = false):
	var east = start
	east.x += 1
	var path = _path_through_territory(owner, east, end, or_unowned)
	if path.size() > 0:
		path.push_front(start)
	return path
	
func pathfind_west(owner, start, end, or_unowned = false):
	var west = start
	west.x -= 1
	var path = _path_through_territory(owner, west, end, or_unowned)
	if path.size() > 0:
		path.push_front(start)
	return path
	