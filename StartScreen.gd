extends Node2D

var start_button

var ask_name
var name_input
var onward

var choose_battle
var one_button
var two_button
var three_button
var four_button
var five_button
var six_button
var seven_button
var eight_button

func _ready():
	start_button = get_node("ViewportContainer/StartButton")
	start_button.connect("button_down", self, "prompt_name")
	ask_name = get_node("ViewportContainer/NamePrompt")
	name_input = get_node("ViewportContainer/NamePrompt/LineEdit")
	onward = get_node("ViewportContainer/NamePrompt/Onward")
	onward.connect("button_down", self, "onward")
	choose_battle = get_node("ViewportContainer/EnemyPrompt")
	one_button = get_node("ViewportContainer/EnemyPrompt/One")
	one_button.connect("button_down", self, "start_one")
	two_button = get_node("ViewportContainer/EnemyPrompt/Two")
	two_button.connect("button_down", self, "start_two")
	three_button = get_node("ViewportContainer/EnemyPrompt/Three")
	three_button.connect("button_down", self, "start_three")
	four_button = get_node("ViewportContainer/EnemyPrompt/Four")
	four_button.connect("button_down", self, "start_four")
	five_button = get_node("ViewportContainer/EnemyPrompt/Five")
	five_button.connect("button_down", self, "start_five")
	six_button = get_node("ViewportContainer/EnemyPrompt/Six")
	six_button.connect("button_down", self, "start_six")
	seven_button = get_node("ViewportContainer/EnemyPrompt/Seven")
	seven_button.connect("button_down", self, "start_seven")
	eight_button = get_node("ViewportContainer/EnemyPrompt/Eight")
	eight_button.connect("button_down", self, "start_eight")
	
func prompt_name():
	ask_name.show()
	
func onward():
	var to_set
	if name_input.text != null && name_input.text.length() > 0:
		to_set = name_input.text
	else:
		to_set = name_input.placeholder_text
		
	var global = get_node("/root/Global")
	global.player_name = to_set
	prompt_battle()
	
func prompt_battle():
	choose_battle.show()
	
func start_one():
	get_tree().change_scene("res://Arenas/OneAI.tscn")
	
func start_two():
	get_tree().change_scene("res://Arenas/TwoAI.tscn")
	
func start_three():
	get_tree().change_scene("res://Arenas/ThreeAI.tscn")
	
func start_four():
	get_tree().change_scene("res://Arenas/FourAI.tscn")
	
func start_five():
	get_tree().change_scene("res://Arenas/FiveAI.tscn")
	
func start_six():
	get_tree().change_scene("res://Arenas/SixAI.tscn")
	
func start_seven():
	get_tree().change_scene("res://Arenas/SevenAI.tscn")
	
func start_eight():
	get_tree().change_scene("res://Arenas/EightAI.tscn")